# Rafael Henrique Gallo

[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=flat&logo=LinkedIn&logoColor=white)](https://www.linkedin.com/in/rafael-gallo-986a73150/)
[![Gmail Badge](https://img.shields.io/badge/-Gmail-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:rafaelhenriquegallo@gmail.com)](mailto:rafaelhenriquegallo@gmail.com)
[![Instagram Badge](https://img.shields.io/badge/-Instagram-C13584?style=flat&logo=Instagram&logoColor=white)](https://www.instagram.com/gallorafaelpy/)
[![Github Badge](https://img.shields.io/badge/-Github-000?style=flat-square&logo=Github&logoColor=white&link=https://github.com/RafaelGallo)](https://github.com/RafaelGallo)
[![GitLab](https://img.shields.io/badge/-GitLab-000?style=flat-square&logo=GitLab&logoColor=white&link=https://gitlab.com/rafael.gallo)](https://gitlab.com/rafael.gallo)
[![Portfolio](https://img.shields.io/badge/-Portfolio-000?style=flat-square&logo=Portfolio&logoColor=grenn&link=https://github.com/RafaelGallo/Portfolio)](https://github.com/RafaelGallo/Portfolio)
[![Site](https://img.shields.io/badge/-Site-000?style=flat-square&logo=Portfolio&logoColor=grenn&link=)]()
[![Article](https://img.shields.io/badge/-Article-000?style=flat-square&logo=Portfolio&logoColor=grenn&link=)]()
[![Origin](https://img.shields.io/badge/-Origin-000?style=flat-square&logo=Origin&logoColor=Orange&link=https://www.origin.com/bra/pt-br/profile/achievements)](https://www.origin.com/bra/pt-br/profile/achievements)
[![Steam](https://img.shields.io/badge/-Steam-000?style=flat-square&logo=Steam&logoColor=white&link=https://steamcommunity.com/profiles/76561198838228349/)](https://steamcommunity.com/profiles/76561198838228349/)

My name is Rafael Gallo, student of computer engineering.

📚 Graduating computer engineering at [IMPACTA](https://www.impacta.edu.br/graduacoes/engenharia-da-computacao)<br>

👨🏻‍💻 Data Scientist in Training, python, machine learning, depp learning.

💻I'm an intern in data Science at Gentrop São Paulo Brazil.

# Skills
* 💻: Python, R, HTML, CSS, JavaScript, Java
* 🖥 : SQL, NoSQL, SQL Server, MongoDB, SQLite
* ☁️: Google cloud, BigQuery, Datalab, Azure, IBM Cloud
* 📁: Data analysis, Machine Learning
* 📊: Data analytics 
* 📙: Calculation 1, Linear algebra
* 📙: Statistical

# My skills
![Windows](https://img.shields.io/badge/-Windows-1E90FF?style=flat-square&logo=windows&logoColor=blue)
![Docker](https://img.shields.io/badge/-Docker-1E90FF?style=flat-square&logo=docker&logoColor=white)
![Power BI](https://img.shields.io/badge/-PowerBI-1E90FF?style=flat-square&logo=microsoft&logoColor=Red)
![Google Data Studio](https://img.shields.io/badge/-GoogleDataStudio-1E90FF?style=flat-square&logo=google&logoColor=white)
![Python](https://img.shields.io/badge/-Python-1E90FF?style=flat-square&logo=python&logoColor=white)
![R](https://img.shields.io/badge/-R-1E90FF?style=flat-square&logo=r&logoColor=white)
![Git](https://img.shields.io/badge/-Git-1E90FF?style=flat-square&logo=git&logoColor=white)
![C](https://img.shields.io/badge/-C-1E90FF?style=flat-square&logo=C&logoColor=white)
![C++](https://img.shields.io/badge/-C++-1E90FF?style=flat-square&logo=C++&logoColor=white)
![Java](https://img.shields.io/badge/-Java-1E90FF?style=flat-square&logo=Java&logoColor=orange)
![HTML5](https://img.shields.io/badge/-HTML5-1E90FF?style=flat-square&logo=HTML5&logoColor=orange)
![CSS](https://img.shields.io/badge/-CSS-1E90FF?style=flat-square&logo=CSS&logoColor=white)
![JavaScript](https://img.shields.io/badge/-JavaScript-1E90FF?style=flat-square&logo=JavaScript&logoColor=orange)
![SQL](https://img.shields.io/badge/-SQL-1E90FF?style=flat-square&logo=SQL&logoColor=white)
![SQLite](https://img.shields.io/badge/-SQLite-1E90FF?style=flat-square&logo=SQLite&logoColor=white)
![SQLServer](https://img.shields.io/badge/-SQLServer-1E90FF?style=flat-square&logo=SQLSERVER&logoColor=white)
![ETL](https://img.shields.io/badge/-ETL-1E90FF?style=flat-square&logo=ETL&logoColor=white)
![MongoDB](https://img.shields.io/badge/-MongoDB-1E90FF?style=flat-square&logo=Mongodb&logoColor=white)
![GitHub](https://img.shields.io/badge/-GitHub-1E90FF?style=flat-square&logo=GitHub&logoColor=white)
![Git](https://img.shields.io/badge/-Git-1E90FF?style=flat-square&logo=Git&logoColor=white)
![MLflow](https://img.shields.io/badge/-MLflow-1E90FF?style=flat-square&logo=MLflow&logoColor=white)
![Google Colab](https://img.shields.io/badge/-Google_Colab-1E90FF?style=flat-square&logo=google&logoColor=white)

<img src="https://img.shields.io/badge/Google-Google-blue?style=for-the-badge&logo=Google-Cloud&logoColor=white" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/BigQuery-blue?style=for-the-badge&logo=Big-Query&logoColor=white" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/Datalab-blue?style=for-the-badge&logo=Data-lab&logoColor=white" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/IBM-IBM Cloud-blue?style=for-the-badge&logo=IBM-CloudlogoColor=white" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/IBM-Watson-blue?style=for-the-badge&logo=IBM-WatsonlogoColor=white" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/Azure-blue?style=for-the-badge&logo=AzurelogoColor=white" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/Azure-Machine-learningblue?style=for-the-badge&logo=Azure-Machine-learninglogoColor=white" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/Editor-VSCode-blue?style=for-the-badge&logo=visual-studio-code&logoColor=white" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/Visual-Studio-blue?style=for-the-badge&logo=Visual-Studio&logoColor=white" />&nbsp;&nbsp;&nbsp;&nbsp;
<br/>
<br/>
## Framework Machine learning and deep learning
<img src="https://img.shields.io/badge/-Cuda-black?style=for-the-badge&logo=Cuda" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Flask-black?style=for-the-badge&logo=flask" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-TensorFlow-181717?style=for-the-badge&logo=TensorFlow" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Keras-181717?style=for-the-badge&logo=Keras" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Theano-181717?style=for-the-badge&logo=Theano" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-PyTorch-181717?style=for-the-badge&logo=PyTorch" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-TensorFlow GPU-black?style=for-the-badge&logo=TensorFlow_GPU" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Yollo-181717?style=for-the-badge&logo=Yollo" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Scikit Learn-181717?style=for-the-badge&logo=Scikit_Learn" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-NLTK-181717?style=for-the-badge&logo=NLTK" />&nbsp;&nbsp;&nbsp;&nbsp;
<br/>
<br/>
## Data analysis
<img src="https://img.shields.io/badge/-Numpy-181717?style=for-the-badge&logo=Numpy" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Matplotlib-181717?style=for-the-badge&logo=Matplotlib" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Seaborn-181717?style=for-the-badge&logo=Seaborn" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Pandas-181717?style=for-the-badge&logo=Pandas" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-ggplot2-181717?style=for-the-badge&logo=ggplot2" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Ploty-181717?style=for-the-badge&logo=Ploty" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-SciPy -181717?style=for-the-badge&logo=SciPy" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Altair -181717?style=for-the-badge&logo=Altair" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Bokeh -181717?style=for-the-badge&logo=Bokeh" />&nbsp;&nbsp;&nbsp;&nbsp;
<br/>
<br/>
## Machine learning
<img src="https://img.shields.io/badge/-Machine learning-181717?style=for-the-badge&logo=Machine_learning" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Regression model-181717?style=for-the-badge&logo=Regression_model" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-classification-181717?style=for-the-badge&logo=classification" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Data analysis-181717?style=for-the-badge&logo=Data_analysis" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-recommendation systems-181717?style=for-the-badge&logo=recommendation_systems" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Supervised algorithms-181717?style=for-the-badge&logo=supervised_algorithms" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-unsupervised algorithms-181717?style=for-the-badge&logo=unsupervised_algorithms" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Supervised natural language model-181717?style=for-the-badge&logo=Supervised_natural_language_model" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Unnatural language process model-181717?style=for-the-badge&logo=Unnatural_language_process_model" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Time series-181717?style=for-the-badge&logo=Time_series" />&nbsp;&nbsp;&nbsp;&nbsp;
<br/>
<br/>
<br/>
### Statistical
<img src="https://img.shields.io/badge/-Statistical-181717?style=for-the-badge&logo=statistical" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Frequencies and averages-181717?style=for-the-badge&logo=frequencies_and_averages" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Probability-181717?style=for-the-badge&logo=Probability" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Sampling-181717?style=for-the-badge&logo=Sampling" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Hypotheses-181717?style=for-the-badge&logo=hypotheses" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Correlations-181717?style=for-the-badge&logo=correlations" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Market basket analysis-181717?style=for-the-badge&logo=Market_basket_analysis" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Linear Regression-181717?style=for-the-badge&logo=Linear_regression" />&nbsp;&nbsp;&nbsp;&nbsp;
<br/>
<br/>
<br/>
### Deep learning
<img src="https://img.shields.io/badge/-ANN Artificial neural network-181717?style=for-the-badge&logo=ANN_Artificial_neural_network" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-CNN Convolutional-181717?style=for-the-badge&logo=ANN_Convolutional" />&nbsp;&nbsp;&nbsp;&nbsp;
<img src="https://img.shields.io/badge/-Computer vision-181717?style=for-the-badge&logo=Computer_Vision" />&nbsp;&nbsp;&nbsp;&nbsp;
